terraform {
  required_version = ">= 0.13"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

variable "user" {
  description = "SSH user"
}

variable "password" {
  description = "SSH user password"
}

  
resource "azurerm_resource_group" "tf_mysql_rg" {
    name     = "myResourceGroup"
    location = "eastus"
}

resource "azurerm_virtual_network" "vnet_tf_mysql" {
    name                = "myVnet"
    address_space       = ["10.80.0.0/16"]
    location            = azurerm_resource_group.tf_mysql_rg.location
    resource_group_name = azurerm_resource_group.tf_mysql_rg.name

    depends_on = [ azurerm_resource_group.tf_mysql_rg ]
}

resource "azurerm_subnet" "subnet_tf_mysql" {
    name                 = "mySubnet"
    resource_group_name  = azurerm_resource_group.tf_mysql_rg.name
    virtual_network_name = azurerm_virtual_network.vnet_tf_mysql.name
    address_prefixes       = ["10.80.4.0/24"]

    depends_on = [ azurerm_resource_group.tf_mysql_rg, azurerm_virtual_network.vnet_tf_mysql ]
}

resource "azurerm_public_ip" "publicip_tf_mysql_db" {
    name                         = "myPublicIPDB"
    location                     = azurerm_resource_group.tf_mysql_rg.location
    resource_group_name          = azurerm_resource_group.tf_mysql_rg.name
    allocation_method            = "Static"
    idle_timeout_in_minutes = 30

    depends_on = [ azurerm_resource_group.tf_mysql_rg ]
}

resource "azurerm_network_interface" "nic_tf_mysql_db" {
    name                      = "myNICDB"
    location                  = azurerm_resource_group.tf_mysql_rg.location
    resource_group_name       = azurerm_resource_group.tf_mysql_rg.name

    ip_configuration {
        name                          = "myNicConfigurationDB"
        subnet_id                     = azurerm_subnet.subnet_tf_mysql.id
        private_ip_address_allocation = "Static"
        private_ip_address            = "10.80.4.10"
        public_ip_address_id          = azurerm_public_ip.publicip_tf_mysql_db.id
    }

    depends_on = [ azurerm_resource_group.tf_mysql_rg, azurerm_subnet.subnet_tf_mysql ]
}

data "azurerm_public_ip" "ip_tf_mysql_data_db" {
  name                = azurerm_public_ip.publicip_tf_mysql_db.name
  resource_group_name = azurerm_resource_group.tf_mysql_rg.name
}

resource "azurerm_storage_account" "storage_tf_mysql_db" {
    name                        = "storagetfmysqldb"
    resource_group_name         = azurerm_resource_group.tf_mysql_rg.name
    location                    = azurerm_resource_group.tf_mysql_rg.location
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    depends_on = [ azurerm_resource_group.tf_mysql_rg ]
}

resource "azurerm_linux_virtual_machine" "vm_tf_mysql_db" {
    name                  = "myVMDB"
    location              = azurerm_resource_group.tf_mysql_rg.location
    resource_group_name   = azurerm_resource_group.tf_mysql_rg.name
    network_interface_ids = [azurerm_network_interface.nic_tf_mysql_db.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDBDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "myvmdb"
    admin_username = var.user
    admin_password = var.password
    disable_password_authentication = false

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storage_tf_mysql_db.primary_blob_endpoint
    }

    depends_on = [ azurerm_resource_group.tf_mysql_rg, azurerm_network_interface.nic_tf_mysql_db, azurerm_storage_account.storage_tf_mysql_db, azurerm_public_ip.publicip_tf_mysql_db ]
}

resource "time_sleep" "wait_30_seconds_db" {
  depends_on = [azurerm_linux_virtual_machine.vm_tf_mysql_db]
  create_duration = "30s"
}

resource "null_resource" "upload_db" {
    provisioner "file" {
        connection {
            type = "ssh"
            user = var.user
            password = var.password
            host = data.azurerm_public_ip.ip_tf_mysql_data_db.ip_address
        }
        source = "mysql"
        destination = "/home/azureuser"
    }

    depends_on = [ time_sleep.wait_30_seconds_db ]
}

resource "null_resource" "deploy_db" {
    triggers = {
        order = null_resource.upload_db.id
    }
    provisioner "remote-exec" {
        connection {
            type = "ssh"
            user = var.user
            password = var.password
            host = data.azurerm_public_ip.ip_tf_mysql_data_db.ip_address
        }
        inline = [
            "sudo apt update",
            "sudo apt install -y mysql-server-5.7",
            "sudo cp -f /home/azureuser/mysql/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf",
            "sudo service mysql restart",
            "sleep 20",
        ]
    }
}

output "public_ip_address" {
  value = azurerm_public_ip.publicip_tf_mysql_db.ip_address
}